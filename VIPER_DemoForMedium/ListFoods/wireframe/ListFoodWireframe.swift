//
//  ListFoodWireframe.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 1/21/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import UIKit

class ListFoodWireFrame : ListFoodWireframeProtocol {
    func pushtoFruitDetail(with food: Food, from view: UIViewController) {
        let foodDetailViewController = view.storyboard?.instantiateViewController(withIdentifier: "FoodDetailView") as! FoodDetailView
        FoodDetailWireFrame.createFoodDetailModule(with: foodDetailViewController, and: food)
        view.navigationController?.pushViewController(foodDetailViewController, animated: true)
    }
    
    
    class func createListFoodModule(foodListref: ListFoodView) {
         let presenter: ListFoodPresenterProtocol & ListFoodInteractorOutputProtocol = ListFoodPresenter()
               
            foodListref.presenter = presenter
            foodListref.presenter?.wireFrame = ListFoodWireFrame()
            foodListref.presenter?.view = foodListref
            foodListref.presenter?.interactor = ListFoodInteractor()
            foodListref.presenter?.interactor?.presenter = presenter
    }

}
