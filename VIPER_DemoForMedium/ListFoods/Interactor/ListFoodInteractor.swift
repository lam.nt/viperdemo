//
//  Interactor.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 1/31/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import Foundation
import UIKit

class ListFoodInteractor: ListFoodInteractorInputProtocol {
    weak var presenter: ListFoodInteractorOutputProtocol?
    
    func getListFood() {
        presenter?.listFoodDidFetch(listFood: getAllFoodDetail())
    }
    
    func getAllFoodDetail () -> [Food] {
        var foodList = [Food]()
        let allFoodDetail = FoodData.generateDataList()
        for item in allFoodDetail {
            foodList.append(Food(attribute: item))
        }
        return foodList
    }
    
}
