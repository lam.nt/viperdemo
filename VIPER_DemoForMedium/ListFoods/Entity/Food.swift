//
//  Entity.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 1/21/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import Foundation
import UIKit

struct Food {
    var name: String!
    var kalo: String!
    init (attribute: [String: String]) {
        self.name = attribute["name"]
        self.kalo = attribute["kalo"]
    }
   
}
class FoodData:NSObject {
    class func generateDataList() -> [[String: String]] {
        return [["name": "banh mi","kalo": "200kalo"],["name": "hu tieu","kalo": "300kalo"], ["name": "rau luot","kalo": "20 kalo"], ["name": "com ga","kalo": "200kalo"]]
    }
}
