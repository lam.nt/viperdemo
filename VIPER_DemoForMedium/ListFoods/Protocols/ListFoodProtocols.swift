//
//  ListFoodProtocols.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 1/21/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import UIKit

protocol ListFoodViewProtocol: class{
    func showFood(with foods: [Food])
}

protocol ListFoodInteractorInputProtocol:class {
    var presenter:ListFoodInteractorOutputProtocol? { get set }
    func getListFood()
}

protocol ListFoodInteractorOutputProtocol: class {
    func listFoodDidFetch(listFood: [Food] )
}

protocol ListFoodPresenterProtocol: class  {
    var view:ListFoodViewProtocol? { get set }
    var interactor:ListFoodInteractorInputProtocol? { get set }
    var wireFrame:ListFoodWireframeProtocol? { get set }
    func showFoodSelection(with food: Food, from view: UIViewController)
    func viewDidLoad()
}

protocol ListFoodWireframeProtocol: class {
    func pushtoFruitDetail(with fruit: Food,from view: UIViewController )
    static func createListFoodModule(foodListref: ListFoodView)
}
