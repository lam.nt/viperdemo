//
//  ListFoodPresenter.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 1/21/20.
//  Copyright © 2020 TL sama. All rights reserved.
//
import UIKit

class ListFoodPresenter: ListFoodPresenterProtocol, ListFoodInteractorOutputProtocol {
    
    func listFoodDidFetch(listFood: [Food]) {
        view?.showFood(with: listFood)
    }
    
    var wireFrame: ListFoodWireframeProtocol?
    var view: ListFoodViewProtocol?
    var interactor: ListFoodInteractorInputProtocol?

    func viewDidLoad() {
        self.loadFoodList()
    }
    func loadFoodList() {
        interactor?.getListFood()
        
    }
    func showFoodSelection(with food: Food, from view: UIViewController) {
        wireFrame?.pushtoFruitDetail(with: food, from: view)
       }


}
