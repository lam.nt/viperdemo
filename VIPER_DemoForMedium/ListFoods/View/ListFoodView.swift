//
//  ListFoodView.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 1/21/20.
//  Copyright © 2020 TL sama. All rights reserved.
//
import UIKit
import Foundation

class ListFoodView: UIViewController, ListFoodViewProtocol {
    
    
    @IBOutlet weak var tableViewFood: UITableView!
    var presenter: ListFoodPresenterProtocol?
    var foodList = [Food]()
    override func viewDidLoad() {
        super.viewDidLoad()
        ListFoodWireFrame.createListFoodModule(foodListref: self)
        presenter?.viewDidLoad()
    }
    
    func showFood(with foods: [Food]) {
        foodList = foods
        tableViewFood.reloadData()
    }
}

//tableView
extension ListFoodView: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewFood.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let food = foodList[indexPath.row]
        cell.textLabel?.text = food.name
        cell.detailTextLabel?.text = food.kalo
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showFoodSelection(with: foodList[indexPath.row], from: self)
    }
    
    
}
