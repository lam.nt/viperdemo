//
//  FoodDetailPresenter.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 2/1/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import Foundation
import UIKit

class FoodDetailPresenter: FoodDetailPresenterProtocol {
    weak var view: FoodDetailViewProtocol?
    
    var wireFrame: FoodDetailWireFrameProtocol?
    var food: Food?
    
    func viewDidLoad() {
        view?.showFoodDetail(with: food!)
    }
    
    func backButtonPressed() {
        
    }
    deinit {
        print("presenter removed")
    }
    
}


    

