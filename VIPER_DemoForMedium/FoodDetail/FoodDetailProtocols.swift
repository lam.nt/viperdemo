//
//  FoodDetailProtocols.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 2/1/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import Foundation
import UIKit

protocol FoodDetailPresenterProtocol: class {
    var view: FoodDetailViewProtocol? { get set }
    var wireFrame: FoodDetailWireFrameProtocol? { get set }
    func viewDidLoad()
    func backButtonPressed ()
    
}

protocol FoodDetailViewProtocol: class {
    func showFoodDetail(with food: Food)
    
}
protocol FoodDetailWireFrameProtocol: class {
    func goBacktoListFoodView(from view: UIViewController)
    
}
