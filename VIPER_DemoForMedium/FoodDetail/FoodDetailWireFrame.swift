//
//  FoodDetailWireFrame.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 2/1/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import Foundation
import UIKit

class FoodDetailWireFrame: FoodDetailWireFrameProtocol {
    class func createFoodDetailModule (with foodDetailRef: FoodDetailView,and food: Food) {
    let presenter = FoodDetailPresenter()
        presenter.food = food
        foodDetailRef.presenter = presenter
        foodDetailRef.presenter?.view = foodDetailRef
        foodDetailRef.presenter?.wireFrame = FoodDetailWireFrame()
        
    
    }
    func goBacktoListFoodView(from view: UIViewController) {
        
    }
    deinit {
        print("wireframe removed")
    }
}
