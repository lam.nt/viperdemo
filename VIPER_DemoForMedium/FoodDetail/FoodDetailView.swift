//
//  FoodDetailView.swift
//  VIPER_DemoForMedium
//
//  Created by TL sama on 2/1/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import Foundation
import UIKit
class FoodDetailView: UIViewController, FoodDetailViewProtocol {

    @IBOutlet weak var foodImage: UIImageView!
    
    @IBOutlet weak var foodName: UILabel!

    @IBOutlet weak var foodVitamin: UILabel!
    var presenter: FoodDetailPresenterProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    func showFoodDetail(with food: Food) {
        foodImage.image = UIImage(named: food.name)
        foodName.text = food.name
        foodVitamin.text = food.kalo
        
    }
    deinit {
        print("view removed")
    }
    
}
